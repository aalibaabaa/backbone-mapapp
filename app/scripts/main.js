/* global Backbone, BackboneMapApp, AppTemplates, Handlebars, console, google */
(function () {
    "use strict";
    window.BackboneMapApp = {

        Models: {
            CityModel: Backbone.Model.extend({
                defaults: function () {
                    return {
                        id: 0,
                        name: 'n.n.',
                        location: '0,0',
                        marker: null
                    };
                },
                url: function () {
                    return 'https://maps.googleapis.com/maps/api/geocode/json?address=' + this.get('q');
                },
                parse: function (response, options) {
                    if (response.status === 'OK') {
                        var res = response.results[0],
                            id = res.geometry.location.lat + "_" + res.geometry.location.lng;
                        this.set({name: res.formatted_address});
                        this.set({location: res.geometry.location});
                        this.set({id:id});
                    }
                }
            })
        },

        Collections: {
            CitiesCollection: Backbone.Collection.extend({
                centerCity: function(cityModelId){
                    if(this.findWhere({id:cityModelId})){
                        this.trigger('centerCity',this.findWhere({id:cityModelId}));
                    }
                }
            })
        },

        Views: {
            MapView: Backbone.View.extend({
                initialize: function (options) {
                    var that = this;
                    that.googleMaps = options.googleMaps;
                    that.googleMapsMap = null;
                    that.CityModel = options.cityModel;
                    that.WarningView = options.WarningView;
                    that._initMapCenterPosition(that._prepareMap);
                    that.collection.on('add',function(city){
                        that._setMapCenter(city);
                        city.set({marker: that._setMapMarker(city)});
                    });
                    that.collection.on('remove', function(city){
                        that._removeMapMarker(city.get('marker'));
                    });
                    that.collection.on('centerCity', function(city){
                        that._toggleMarkerBounce(city.get('marker'));
                        that._setMapCenter(city);
                    });
                },

                _initMapCenterPosition: function(callback){
                    var that = this;
                    if(navigator.geolocation){
                        var geoOptions = {
                            enableHighAccuracy: true,
                            timeout: 10000,
                            maximumAge: 0
                        };
                        navigator.geolocation.getCurrentPosition(
                            function(position){
                                return callback(null,position, that);
                            },
                            function(error){
                                return callback(error, null, that);
                            },
                            geoOptions
                        );
                    } else {
                        return callback(new Error('No Geolocation API available'), null, that);
                    }
                },

                _prepareMap: function (error, position, scope) {
                    var that = scope,
                        cityModel = new that.CityModel(),
                        lat,lng,
                        mapOptions = {
                            zoom: 12,
                            mapTypeId: that.googleMaps.MapTypeId.ROADMAP,
                            center: null
                        };

                    if(error){
                        // Default for centering Map: Zürich Enge
                        lat = 47.3640548;
                        lng = 8.528543299999999;
                    } else {
                        lat = position.coords.latitude;
                        lng = position.coords.longitude;
                    }

                    mapOptions.center = new that.googleMaps.LatLng(lat,lng);

                    //Show map and center it
                    that.googleMapsMap = new that.googleMaps.Map(document.getElementById('map-view'), mapOptions);

                },

                _setMapCenter: function(model){
                    var city = model.toJSON(),
                        latLong = new this.googleMaps.LatLng(city.location.lat,city.location.lng);

                    this.googleMapsMap.panTo(latLong);
                },

                _setMapMarker: function(model){
                    var city = model.toJSON(),
                        latLong = new this.googleMaps.LatLng(city.location.lat,city.location.lng),
                        that = this;

                    return new this.googleMaps.Marker({
                        position: latLong,
                        map: that.googleMapsMap,
                        title: city.name
                    });
                },

                _removeMapMarker: function(marker){
                    var that = this;
                    marker.setMap(null);

                    if(that.collection.models[0]){
                        that._setMapCenter(that.collection.models[0]);
                    }
                },

                _toggleMarkerBounce: function(marker){
                    var that = this;

                    //first, stop all other bouncing markers
                    that.collection.each(function(city){
                        if(city.get('marker') !== marker){
                            city.get('marker').setAnimation(null);
                        }
                    });

                    if (marker.getAnimation()) {
                        marker.setAnimation(null);
                    } else {
                        marker.setAnimation(that.googleMaps.Animation.BOUNCE);
                        // Stop bouncing after 3 seconds
                        setTimeout(function(){
                            marker.setAnimation(null);
                        }, 3000);
                    }
                }
            }),

            SearchView: Backbone.View.extend({
                initialize: function (options) {
                    this.template = options.template;
                    this.render();
                    this.googleMaps = options.googleMaps;
                    var that = this;
                },
                events: {
                    'click .js-search-button': '_searchCity',
                    'keyup .js-search-input': '_searchByEnterKey'
                },
                render: function () {
                    var template = Handlebars.compile(this.template);
                    this.$el.html(template());
                },
                _searchByEnterKey: function (e) {
                    if (e.keyCode === 13) {
                        this._searchCity();
                    }
                },
                _searchCity: function () {
                    var $input = this.$el.find('.js-search-input');
                    if ($input.val().length < 3) {
                        return false;
                    }
                    var cityModel = new this.model(),
                        that = this;
                    cityModel.set({q: $input.val()});
                    cityModel.fetch({
                        success: function (model) {
                            that.collection.add(model,{merge:true});
                            $input.val('');
                        },
                        error: function (model, response, options) {
                            console.error(model, response, options);
                        }
                    });
                }
            }),

            ListView: Backbone.View.extend({
                initialize: function (options) {
                    var that = this;
                    that.template = options.template;
                    that.ListView = options.ListView;
                    that.collection.on('add remove', function () {
                        that._reRenderList();
                    });
                    that.render();
                },
                events: {
                    'click .js-list-item-del': '_deleteItem',
                    'click .js-list-item-center': '_centerItem'
                },
                render: function () {
                    var template = Handlebars.compile(this.template),
                        data = {countCities: this.collection.length};
                    this.$el.html(template(data));
                },
                _reRenderList: function () {
                    var $countcities = this.$el.find('.js-count-cities');
                    $countcities.html(this.collection.length);
                    this._renderList();
                },
                _renderList: function () {
                    var $ul = this.$el.find('.item-list').html(''),
                        that = this;

                    this.collection.each(function (city) {
                        var view = new that.ListView({
                            model: city,
                            collection: that.collection
                        });
                        $ul.append(view.render());
                    });
                },
                _deleteItem: function (e) {
                    e.preventDefault();
                    var id = $(e.target).parent().data('model-id'),
                        model = this.collection.findWhere({id:id});

                    if(model){
                        this.collection.remove(model);
                    } else {
                        console.log('could not remove model with id ' + id);
                    }
                },

                _centerItem: function (e) {
                    e.preventDefault();
                    var id = $(e.target).closest("[data-model-id]").data('model-id');

                    if(id){
                        this.collection.centerCity(id);
                    }
                }
            }),

            ItemView: Backbone.View.extend({
                initialize: function (options) {
                    this.template = AppTemplates.get('listitem');
                    this.render();
                },
                render: function () {
                    var template = Handlebars.compile(this.template),
                        model = this.model.toJSON();
                    return template(model);
                }
            })
        }
    };

})();

$(document).ready(function () {
    "use strict";

    var CitiesCollection = new BackboneMapApp.Collections.CitiesCollection();

    var mapView = new BackboneMapApp.Views.MapView({
        googleMaps: google.maps,
        cityModel: BackboneMapApp.Models.CityModel,
        collection: CitiesCollection,
        WarningView: BackboneMapApp.Views.WarningView
    });

    var searchView = new BackboneMapApp.Views.SearchView({
        el: '#search-view',
        template: AppTemplates.get('search'),
        model: BackboneMapApp.Models.CityModel,
        collection: CitiesCollection,
        googleMaps: google.maps
    });

    var listView = new BackboneMapApp.Views.ListView({
        el: '#list-view',
        template: AppTemplates.get('list'),
        collection: CitiesCollection,
        ListView: BackboneMapApp.Views.ItemView
    });
});
