window.AppTemplates = {

    _search: '<div class="input-group">' +
                '<input type="text" class="form-control js-search-input">' +
                '<span class="input-group-btn">' +
                    '<button class="btn btn-default js-search-button" type="button">Go!</button>' +
                '</span>' +
            '</div>',

    _list: '<h2><span class="js-count-cities">{{countCities}}</span> places selected</h2>' +
        '<ul class="item-list"></ul>',

    _listitem: '<li data-model-id="{{id}}"><a href="#" class="js-list-item-del list-item-del glyphicon glyphicon-remove-circle"></a><a href="#" class="js-list-item-center"><strong>{{name}}</strong></a></li>',

    _offlinewarning: '<div class="row"><div class="label label-warning col-lg-12">Achtung: Die Seite scheint offline zu sein ' +
        'und stellt deshalb nicht alle Features zur Verfügung</div></div>',

    get: function(templateName){
        'use strict';
        if (this['_' + templateName.toLowerCase()]){
            return this['_' + templateName.toLowerCase()];
        }
        return {};
    }
};
